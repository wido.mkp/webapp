import ProfilePage from "@/views/ProfilePage/index";

const ProfileScreen = () => {
    return (
        <ProfilePage />
    );
};

export default ProfileScreen;