import RegisterVehiclePage from "@/views/VehiclePage/RegisterVehiclePage";

const RegisterVehicleScreen = () => {
    return (
        <RegisterVehiclePage />
    );
};

export default RegisterVehicleScreen;