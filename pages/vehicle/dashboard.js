import VehicleDashboardPage from "@/views/VehiclePage/DashboardVehiclePage/index";

const DashboardVehicleScreen = () => {
    return (
        <VehicleDashboardPage />
    );
};

export default DashboardVehicleScreen;