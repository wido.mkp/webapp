import RegisterPage from "@/views/AuthPage/RegisterPage";

const RegisterScreen = () => {
    return (
        <RegisterPage />
    );
};

export default RegisterScreen;