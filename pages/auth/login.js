import LoginPage from "@/views/AuthPage/LoginPage";

const LoginScreen = () => {
    return (
        <LoginPage />
    );
};

export default LoginScreen;