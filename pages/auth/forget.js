import ForgetPage from "@/views/AuthPage/ForgetPage";

const ForgetScreen = () => {
    return (
        <ForgetPage />
    );
};

export default ForgetScreen;