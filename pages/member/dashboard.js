import DashboardMemberPage from "@/views/MemberPage/DashboardMember/index";

const DashboardMemberScreen = () => {
    return (
        <DashboardMemberPage />
    );
};

export default DashboardMemberScreen;