import RegisterMemberPage from "@/views/MemberPage/RegisterMember/index";

const RegisterMemberScreen = () => {
    return (
        <RegisterMemberPage />
    );
};

export default RegisterMemberScreen;