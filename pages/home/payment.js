import PaymentPage from "@/views/PaymentPage/index";

const PaymentScreen = () => {
    return (
        <PaymentPage />
    );
};

export default PaymentScreen;