import HistoryPage from "@/views/HistoryPage/HistoryPage";

const HistoryScreen = () => {
    return (
        <HistoryPage />
    );
};

export default HistoryScreen;