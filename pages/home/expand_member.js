import ExpandMemberPage from "@/views/MemberPage/ExpandMember/index";

const ExpandMemberScreen = () => {
    return (
        <ExpandMemberPage />
    );
};

export default ExpandMemberScreen;