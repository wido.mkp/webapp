import HomePage from "@/views/HomePage/HomePage";

const HomeScreen = () => {
    return (
        <HomePage />
    );
};

export default HomeScreen;