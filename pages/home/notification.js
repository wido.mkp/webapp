import NotificationPage from "@/views/NotificationPage/index";

const NotificationScreen = () => {
    return (
        <NotificationPage />
    );
};

export default NotificationScreen;