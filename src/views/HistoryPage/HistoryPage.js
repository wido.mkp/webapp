import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./History.module.js";
import HistoryCardItem from "@/components/HistoryCardItem";
import SelectModule from "@/components/Select";
import MainLayout from "@/containers/MainLayout";
import Button from '@mui/material/Button';
import { Grid } from "@mui/material";
import { useTheme } from "@mui/styles";

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];

const cardData = [1, 2, 4, 4, 6, 7];
const ExpandComponent = () => {
    return (
        <Grid style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            width: "90%"
        }}>
            <Button
                variant="contained"
                style={{
                    width: "95px",
                    height: "28px",
                    backgroundColor: "#11D400",
                    fontSize: "11px",
                    textTransform: "capitalize",
                    color: "white",
                    borderRadius: "6px"
                }}
            >
                Beli Lagi
            </Button>
            <Grid style={{
                fontSize: "11px",
                textTransform: "capitalize",
                color: "#2B4CAB",
                fontWeight: "bold"
            }}>Rp 75.000</Grid>
        </Grid>
    )
}
const HistoryPage = () => {
    const [personName, setPersonName] = React.useState([]);
    const theme = useTheme();
    const styles = useStyles(theme);
    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    return (
        <>
            <Head>
                <title>History Member Parking</title>
            </Head>
            <MainLayout>
                <Grid style={{
                    fontFamily: "Poppins",
                    fontSize: "14px",
                    color: "#2B4CAB",
                    fontWeight: "bold",
                    marginTop: "70px"
                }}>History Pembelian</Grid>
                <Grid sx={styles.historySelect}>
                    <SelectModule
                        label="Semua Tanggal"
                        data={names}
                        value={personName}
                        onChange={handleChange}
                        width={137.38}
                        style={{ height: "31px", fontSize: "9px" }}
                    />
                    <SelectModule
                        label="Semua Kendaraan"
                        data={names}
                        value={personName}
                        width={137.38}
                        onChange={handleChange}
                        style={{ height: "31px", fontSize: "9px" }}
                    />
                </Grid>
                <Grid style={{ marginTop: "21px", marginBottom: "70px" }}>
                    {
                        cardData.map((data, i) => {
                            return (
                                <HistoryCardItem
                                    key={i} vehicleName="avanza"
                                    date="22/12/2019"
                                    isCollapse={true}
                                    expandComponent={<ExpandComponent />}
                                >
                                    <Grid style={{
                                        display: "flex",
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        width: "90%"
                                    }}>
                                        <Grid>ID. 9310000313881</Grid>
                                        <Grid style={{
                                            color: "#11D400",
                                            fontFamily: "Poppins",
                                            fontSize: "9px",
                                            fontWeight: "bold"
                                        }}>Berhasil</Grid>
                                    </Grid>
                                    <Grid>
                                        Avanza H 1234 KL
                                    </Grid>
                                    <Grid>
                                        DP Mall jl. Lorem Ipsum dolor sit amet,
                                    </Grid>
                                    <Grid>
                                        consectetur adipiscing elit......
                                    </Grid>
                                </HistoryCardItem>
                            );
                        })
                    }
                </Grid>
            </MainLayout>
        </>
    );
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(HistoryPage);