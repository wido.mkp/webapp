import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./DashboardMember.module.js";
import MainLayout from "@/containers/MainLayout";
import Image from "next/image";
import Divider from "@/components/Divider";
import SelectModule from "@/components/Select";
import Button from '@mui/material/Button';
import HistoryCardItem from "@/components/HistoryCardItem";
import Router from "next/router";
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];
const cardData = [1, 2, 4, 4, 6, 7];
const ExpandComponent = () => {
    return (
        <Grid style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            width: "90%"
        }}>
            <Grid style={{
                display: "flex",
                flexDirection: "column",
                fontFamily: "Poppins",
                fontSize: "9px"
            }}>
                <Grid style={{ color: "#2B4CAB", fontWeight: "bold" }}>Member 3 Bulan</Grid>
                <Grid>exp. date 30/03/2022</Grid>
                <Grid style={{ color: "#D90000" }}>3 days left</Grid>
            </Grid>
            <Button
                variant="contained"
                style={{
                    width: "95px",
                    height: "28px",
                    backgroundColor: "#11D400",
                    fontSize: "11px",
                    textTransform: "capitalize",
                    color: "white",
                    borderRadius: "6px"
                }}
            >
                Perpanjang
            </Button>
        </Grid>
    )
}
const DashboardMemberPage = () => {
    const [personName, setPersonName] = React.useState([]);
    const theme = useTheme();
    const styles = useStyles(theme);
    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    const goAddMember = () => {
        Router.push("/member/register");
    }
    return (
        <>
            <Head>
                <title>Dashboard Member Parking</title>
            </Head>
            <MainLayout>
                <Grid style={{ marginTop: "55px" }}>
                    <Grid sx={styles.labelMember}>
                        Member
                    </Grid>
                    <Grid sx={styles.statusMemberWrapper}>
                        <Grid sx={styles.boxImage}>
                            <Image src="/images/car.png" width={86.1} height={46} layout="fixed" />
                        </Grid>
                        <Grid sx={styles.boxStatus}>
                            <Grid sx={styles.statusLabel}>Status Member</Grid>
                            <Grid sx={styles.statusItemWrapper}>
                                <Grid sx={styles.circleItemActive} />
                                <Grid sx={styles.itemVehicle}>23 Member Active</Grid>
                            </Grid>
                            <Grid sx={styles.statusItemWrapper}>
                                <Grid sx={styles.circleItemAlmost} />
                                <Grid sx={styles.itemVehicle}>5 Member Almost Expired</Grid>
                            </Grid>
                            <Grid sx={styles.statusItemWrapper}>
                                <Grid sx={styles.circleItemExpired} />
                                <Grid sx={styles.itemVehicle}>9 Member Expired</Grid>
                            </Grid>
                            <Button
                                variant="contained"
                                style={{
                                    width: "134px",
                                    height: "31px",
                                    backgroundColor: "#2B4CAB",
                                    fontSize: "10px",
                                    textTransform: "capitalize",
                                    color: "white",
                                    borderRadius: "6px",
                                    position: "absolute",
                                    bottom: 0,
                                    left: 0
                                }}
                                onClick={() => goAddMember()}
                            >
                                + Tambah Member
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid style={{ marginLeft: "-40px", marginRight: "-40px", marginBottom: "35px" }}>
                        <Divider height="6px" />
                    </Grid>
                    <Grid sx={styles.labelMember}>
                        Detail Member
                    </Grid>
                    <Grid style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginBottom: "23px",
                    }}>
                        <SelectModule
                            label="Semua Kendaraan"
                            data={names}
                            value={personName}
                            width="137.38px"
                            onChange={handleChange}
                            style={{ height: "31px", fontSize: "9px" }}
                        />
                        <SelectModule
                            label="Semua Status"
                            data={names}
                            value={personName}
                            width="137.38px"
                            onChange={handleChange}
                            style={{ height: "31px", fontSize: "9px" }}
                        />
                    </Grid>
                    <Grid style={{ marginBottom: "80px" }}>
                        {
                            cardData.map((data, i) => {
                                return (
                                    <HistoryCardItem
                                        key={i}
                                        vehicleName="avanza"
                                        isShowDate={false}
                                        icon="motorcycle"
                                        isCollapse={true}
                                        expandComponent={<ExpandComponent />}
                                    >
                                        <Grid style={{
                                            display: "flex",
                                            flexDirection: "row",
                                            justifyContent: "space-between",
                                            width: "90%"
                                        }}>
                                            <Grid>ID. 9310000313881</Grid>
                                            <Grid style={{color: "#E9EE00", fontWeight: "bold"}}>Almost Expired</Grid>
                                        </Grid>
                                        <Grid>
                                            Avanza H 1234 KL
                                        </Grid>
                                        <Grid>
                                            H 1234 KL
                                        </Grid>
                                    </HistoryCardItem>
                                );
                            })
                        }
                    </Grid>
                </Grid>
            </MainLayout>
        </>
    )
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(DashboardMemberPage);