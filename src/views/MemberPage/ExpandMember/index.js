import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./ExpandMember.module.js";
import Button from '@mui/material/Button';
import TextFieldModule from "@/components/TextField";
import MainLayout from "@/containers/MainLayout";
import HistoryCardItem from "@/components/HistoryCardItem";
import DatePickerModule from "@/components/DatePicker";
import { Grid } from "@mui/material";
import { useTheme } from "@mui/styles";

const ExpandMemberPage = () => {
    const [duration, setDuration] = React.useState(0)
    const [value, setValue] = React.useState(null);
    const theme = useTheme();
    const styles = useStyles(theme);
    return (
        <>
            <Head>
                <title>Register Member Parking</title>
            </Head>
            <MainLayout>
                <Grid style={{ marginTop: "58px" }}>
                    <Grid sx={styles.labelMember}>
                        Perpanjang Member
                    </Grid>
                    <Grid style={{ marginTop: "30px" }}>
                        <HistoryCardItem vehicleName="avanza" isShowDate={false}>
                            <Grid style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-between",
                                width: "90%"
                            }}>
                                <Grid>
                                    ID. 9310000313881
                                </Grid>
                                <Grid style={{
                                    color: "#E9EE00"
                                }}>
                                    Almost Expired
                                </Grid>
                            </Grid>
                            <Grid>
                                Avanza H 1234 KL
                            </Grid>
                            <Grid>
                                DP Mall jl. Lorem Ipsum dolor sit amet,
                            </Grid>
                            <Grid>
                                consectetur adipiscing elit......
                            </Grid>
                            <Grid style={{
                                marginTop: "10px",
                                fontFamily: "Poppins",
                                color: "#2B4CAB",
                                fontWeight: "bold"
                            }}>
                                Member 3 Bulan
                            </Grid>
                            <Grid>
                                Expired Date 30/12/2024
                            </Grid>
                        </HistoryCardItem>
                    </Grid>
                    <Grid style={{ marginTop: "42px" }}>
                        <Grid sx={styles.labelMember}>
                            Jenis Membership
                        </Grid>
                        <Grid sx={styles.typeMembership}>
                            <Button
                                variant="contained"
                                style={{
                                    width: "90px",
                                    height: "48px",
                                    backgroundColor: duration === 0 ? "#2B4CAB" : "white",
                                    fontSize: "10px",
                                    textTransform: "capitalize",
                                    color: duration === 0 ? "white" : "black",
                                    borderRadius: "6px"
                                }}
                                onClick={() => setDuration(0)}
                            >
                                Minggu
                            </Button>
                            <Button
                                variant="contained"
                                style={{
                                    width: "90px",
                                    height: "48px",
                                    backgroundColor: duration === 1 ? "#2B4CAB" : "white",
                                    fontSize: "10px",
                                    textTransform: "capitalize",
                                    color: duration === 1 ? "white" : "black",
                                    borderRadius: "6px"
                                }}
                                onClick={() => setDuration(1)}
                            >
                                Bulan
                            </Button>
                            <Button
                                variant="contained"
                                style={{
                                    width: "90px",
                                    height: "48px",
                                    backgroundColor: duration === 2 ? "#2B4CAB" : "white",
                                    fontSize: "10px",
                                    textTransform: "capitalize",
                                    color: duration === 2 ? "white" : "black",
                                    borderRadius: "6px"
                                }}
                                onClick={() => setDuration(2)}
                            >
                                Tahun
                            </Button>
                        </Grid>
                        <Grid sx={styles.totalPembayaran}>
                            <Grid sx={styles.labelPembayaran}>Total Pembayaran</Grid>
                            <Grid sx={styles.labelPrice}>Rp 100000,-</Grid>
                        </Grid>
                        <Grid sx={styles.typeMembership}>
                            <Grid>
                                {/* <Grid style={{
                                    fontSize: "10px",
                                    fontFamily: "Poppins",
                                    color: "#2B4CAB",
                                    fontWeight: "bold",
                                    marginBottom: "5px"
                                }}>Mulai Member</Grid> */}
                                <TextFieldModule
                                    hiddenLabel={true}
                                    variant="outlined"
                                    label="Mulai Member"
                                    fullWidth={true}
                                    type="date"
                                    width={130}
                                />
                            </Grid>
                            <Grid>
                                {/* <Grid style={{
                                    fontSize: "10px",
                                    fontFamily: "Poppins",
                                    color: "#2B4CAB",
                                    fontWeight: "bold",
                                    marginBottom: "5px"
                                }}>Grace Period</Grid> */}
                                <Grid style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    position: 'relative'
                                }}>
                                    <TextFieldModule
                                        hiddenLabel={true}
                                        variant="outlined"
                                        label="Grace Period"
                                        fullWidth={false}
                                        type="number"
                                        width={130}
                                    />
                                    <Grid style={{
                                        position: 'absolute',
                                        right: 10,
                                        fontSize: "10px",
                                        textTransform: "capitalize",
                                        fontWeight: 'bold'
                                    }}>Hari</Grid>
                                </Grid>
                                {/* <input
                                    sx={styles.customInput}
                                    placeholder="Grace Period"
                                /> */}
                            </Grid>
                        </Grid>
                        <Grid style={{ marginTop: "46px" }}>
                            <Grid style={{
                                marginTop: "74px",
                                marginBottom: "44px",
                                width: "100%",
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center"
                            }}>
                                <Button
                                    variant="contained"
                                    style={{
                                        width: "184px",
                                        height: "42px",
                                        backgroundColor: "#2B4CAB",
                                        fontSize: "10px",
                                        textTransform: "capitalize",
                                        color: "white",
                                        borderRadius: "6px"
                                    }}
                                >
                                    OK
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </MainLayout>
        </>
    );
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(ExpandMemberPage);