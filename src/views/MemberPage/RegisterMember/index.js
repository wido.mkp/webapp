import React, { useCallback, useState, useEffect } from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./RegisterMember.module.js";
import SelectModule from "@/components/Select";
import TextFieldModule from "@/components/TextField";
import Button from '@mui/material/Button';
import Image from "next/image";
import MainLayout from "@/containers/MainLayout";
import Router from "next/router";
import { useTheme } from "@mui/styles";
import { Grid, Box } from "@mui/material";
import Modal from '@mui/material/Modal';

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "300px",
    bgcolor: 'background.paper',
    boxShadow: 24,
    display: 'flex',
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "10px",
    p: 4,
};
const RegisterMember = (props) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [personName, setPersonName] = React.useState([]);
    const [duration, setDuration] = React.useState(0)
    const [value, setValue] = React.useState(new Date('2014-08-18T21:11:54'));
    const [open, setOpen] = React.useState(false);
    const [message, setMessage] = useState(null);
    const scan = useCallback(async () => {
        if (typeof window !== "undefined" && typeof navigator !== "undefined") {
            let userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                setMessage("Sistem operasi perangkat anda tidak mendukung!");
            } else {
                if ('NDEFReader' in window) {
                    try {
                        const ndef = new window.NDEFReader();
                        await ndef.scan();
                        ndef.onreadingerror = () => {
                            setMessage("Tidak dapat membaca NFC Tag, coba sekali lagi?");
                        };

                        ndef.onreading = async (event) => {
                            let serialNumber = event.serialNumber.split(":").join("")
                            if (serialNumber.length < 14) {
                                let lessSerialNumber = 14 - serialNumber.length;
                                let data = ""
                                for (let i = 1; i <= lessSerialNumber; i++) {
                                    data += "0";
                                }
                                setMessage(data + serialNumber.toUpperCase());
                            } else {
                                setMessage(serialNumber.toUpperCase());
                            }
                        };

                    } catch (error) {
                        setMessage(error);
                    };
                }
            }
        }
    }, [message])

    React.useEffect(() => {
        if (open) {
            setMessage(null);
            scan();
        }
    }, [open]);

    const handleOpen = () => {
        setOpen(true)
        scan();
    };

    const handleClose = () => {
        setMessage(null);
        setOpen(false);
    };

    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    const goPaymentPage = () => {
        Router.push("/home/payment");
    }

    return (
        <>
            <Head>
                <title>Register Member Parking</title>
            </Head>
            <MainLayout disableNav={true}>
                <Grid style={{ marginTop: "53.4px" }}>
                    <Grid sx={styles.labelMember}>
                        Data Kendaraan
                    </Grid>
                    <SelectModule
                        label="Jenis Kendaraan"
                        data={names}
                        value={personName}
                        width="100%"
                        onChange={handleChange}
                        style={{ height: "31px", fontSize: "9px" }}
                    />
                    <SelectModule
                        label="Lokasi Membership"
                        data={names}
                        value={personName}
                        width="100%"
                        onChange={handleChange}
                        style={{ height: "31px", fontSize: "9px" }}
                    />
                    <SelectModule
                        label="No Plat"
                        data={names}
                        value={personName}
                        width="100%"
                        onChange={handleChange}
                        style={{ height: "31px", fontSize: "9px" }}
                    />
                    <Grid style={{ marginTop: "42px" }}>
                        <Grid sx={styles.labelMember}>
                            Jenis Membership
                        </Grid>
                        <Grid sx={styles.typeMembership}>
                            <Button
                                variant="contained"
                                style={{
                                    width: "90px",
                                    height: "48px",
                                    backgroundColor: duration === 0 ? "#2B4CAB" : "white",
                                    fontSize: "10px",
                                    textTransform: "capitalize",
                                    color: duration === 0 ? "white" : "black",
                                    borderRadius: "6px"
                                }}
                                onClick={() => setDuration(0)}
                            >
                                Minggu
                            </Button>
                            <Button
                                variant="contained"
                                style={{
                                    width: "90px",
                                    height: "48px",
                                    backgroundColor: duration === 1 ? "#2B4CAB" : "white",
                                    fontSize: "10px",
                                    textTransform: "capitalize",
                                    color: duration === 1 ? "white" : "black",
                                    borderRadius: "6px"
                                }}
                                onClick={() => setDuration(1)}
                            >
                                Bulan
                            </Button>
                            <Button
                                variant="contained"
                                style={{
                                    width: "90px",
                                    height: "48px",
                                    backgroundColor: duration === 2 ? "#2B4CAB" : "white",
                                    fontSize: "10px",
                                    textTransform: "capitalize",
                                    color: duration === 2 ? "white" : "black",
                                    borderRadius: "6px"
                                }}
                                onClick={() => setDuration(2)}
                            >
                                Tahun
                            </Button>
                        </Grid>
                        <Grid sx={styles.totalPembayaran}>
                            <Grid sx={styles.labelPembayaran}>Total Pembayaran</Grid>
                            <Grid sx={styles.labelPrice}>Rp 100000,-</Grid>
                        </Grid>
                        <Grid sx={styles.typeMembership}>
                            <Grid>
                                {/* <Grid style={{
                                    fontSize: "10px",
                                    fontFamily: "Poppins",
                                    color: "#2B4CAB",
                                    fontWeight: "bold",
                                    marginBottom: "5px"
                                }}>Mulai Member</Grid> */}
                                <TextFieldModule
                                    hiddenLabel={true}
                                    variant="outlined"
                                    label="Mulai Member"
                                    fullWidth={true}
                                    type="date"
                                    width={130}
                                />
                            </Grid>
                            <Grid>
                                {/* <Grid style={{
                                    fontSize: "10px",
                                    fontFamily: "Poppins",
                                    color: "#2B4CAB",
                                    fontWeight: "bold",
                                    marginBottom: "5px"
                                }}>Grace Period</Grid> */}
                                <Grid style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    position: 'relative'
                                }}>
                                    <TextFieldModule
                                        hiddenLabel={true}
                                        variant="outlined"
                                        label="Grace Period"
                                        fullWidth={false}
                                        type="number"
                                        width={130}
                                    />
                                    <Grid style={{
                                        position: 'absolute',
                                        right: 10,
                                        fontSize: "10px",
                                        textTransform: "capitalize",
                                        fontWeight: 'bold'
                                    }}>Hari</Grid>
                                </Grid>
                                {/* <input
                                    sx={styles.customInput}
                                    placeholder="Grace Period"
                                /> */}
                            </Grid>
                        </Grid>
                        <Grid style={{ marginTop: "46px" }}>
                            <Grid sx={styles.labelMember}>
                                Metode Aktivasi Membership
                            </Grid>
                            <Grid sx={styles.typeMembership}>
                                <Button
                                    variant="contained"
                                    style={{
                                        width: "134px",
                                        height: "48px",
                                        backgroundColor: "white",
                                        fontSize: "10px",
                                        textTransform: "capitalize",
                                        color: "black",
                                        borderRadius: "6px",
                                        display: "flex",
                                        flexDirection: "row",
                                        justifyContent: "flex-start",
                                        alignItems: "center"
                                    }}
                                    onClick={() => handleOpen()}
                                >
                                    <Image src="/images/nfc.png" width={18} height={18} layout="fixed" />
                                    <Grid style={{ fontSize: "10px", fontFamily: "Poppins", fontWeight: "bold", marginLeft: "5px" }}>HP NFC</Grid>
                                </Button>
                                <Button
                                    variant="contained"
                                    style={{
                                        width: "134px",
                                        height: "48px",
                                        backgroundColor: "white",
                                        fontSize: "10px",
                                        textTransform: "capitalize",
                                        color: "black",
                                        borderRadius: "6px",
                                        display: "flex",
                                        flexDirection: "row",
                                        justifyContent: "flex-start",
                                        alignItems: "center"
                                    }}
                                >
                                    <Image src="/images/onsite.png" width={18} height={18} layout="fixed" />
                                    <Grid style={{ fontSize: "10px", fontFamily: "Poppins", fontWeight: "bold", marginLeft: "5px" }}>ON SITE OFFICE</Grid>
                                </Button>
                            </Grid>
                            <Grid style={{ marginTop: "51px" }}>
                                <Grid sx={styles.contentInfo}>
                                    <p>Note :</p>
                                    Parking Membership adalah kartu prepaid sebagai pengganti transaksi parkir di sejumlah titik lokasi parkir yang sudah terintegrasi dengan Parking Member.
                                    <p>
                                        <b>Handphone NFC</b> ditujukan kepada anda yang ingin langsung mengintegrasikan parking membership ke handphone yang sudah didukung dengan teknologi NFC.
                                    </p>
                                    <p>
                                        <b>Onsite Office</b> ditujukan kepada anda yang ingin mengambil sendiri kartu member di sejumlah titik lokasi parkir yang sudah terintegrasi dengan Parking Membership / jika anda ingin mengalami kehilangan kartu member dan ingin mengganti dengan kartu baru.
                                    </p>
                                </Grid>
                            </Grid>
                            <Grid style={{
                                marginTop: "45px",
                                marginBottom: "43px",
                                width: "100%",
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center"
                            }}>
                                <Button
                                    variant="contained"
                                    style={{
                                        width: "184px",
                                        height: "42px",
                                        backgroundColor: "#2B4CAB",
                                        fontSize: "10px",
                                        textTransform: "capitalize",
                                        color: "white",
                                        borderRadius: "6px"
                                    }}
                                    onClick={() => goPaymentPage()}
                                >
                                    OK
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Grid sx={style}>
                        {
                            !message ? <>
                                <img src="/images/loading.gif" style={{ width: "100px", height: "100px" }} />
                                <span style={{
                                    fontSize: "14px",
                                    fontFamily: "Poppins",
                                    color: "#2B4CAB",
                                    fontWeight: "bold",
                                    marginBottom: "5px",
                                    marginTop: "20px"
                                }}>Tag NFC Anda!</span>
                            </> : <>
                                <Grid sx={{
                                    padding: "5px",
                                    display: 'flex',
                                    flexDirection: "row",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    fontSize: "14px",
                                    fontFamily: "Poppins",
                                    color: "black",
                                    fontWeight: "bold",
                                }}>{message}</Grid>
                                <span style={{
                                    fontSize: "14px",
                                    fontFamily: "Poppins",
                                    color: "#2B4CAB",
                                    fontWeight: "bold",
                                    marginBottom: "5px",
                                    marginTop: "20px"
                                }}>Result!</span>
                            </>
                        }
                    </Grid>
                </Modal>
            </MainLayout>
        </>
    );
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(RegisterMember);