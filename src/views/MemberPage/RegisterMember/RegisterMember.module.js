const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    labelMember:{
        fontFamily: "Poppins",
        fontSize: "14px",
        color: "#2B4CAB",
        fontWeight: "bold",
        marginBottom: "20px",
    },
    typeMembership:{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: "20px",
        marginBottom: "46px",
    },
    totalPembayaran:{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
    },
    labelPembayaran:{
        color: "#2B4CAB",
        fontSize: "10px",
        fontFamily: "Poppins",
        marginRight: "10%",
    },
    labelPrice:{
        fontSize: "16px",
        color: "#2B4CAB",
        fontFamily: "Poppins",
        fontWeight: "bold",
    },
    contentInfo:{
        display: "block",
        fontFamily: "Poppins",
        fontSize: "9px",
        textAlign: "left",
    },
    customInput:{
        width: "125px",
        height: "47px",
        fontSize: "10px",
        paddingLeft: "12px",
        paddingRight: "12px",
        borderRadius: "6px",
        border: "none",
        boxShadow: "0.1px 2px #888888",
        '&:focus': {
            border: "none",
            outline: "none !important",
          },
    },
});
export default useStyles;