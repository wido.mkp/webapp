import React from "react";
import { connect } from "react-redux";
import MainLayout from "@/containers/MainLayout";
import Head from "next/head";
import Image from "next/image";
import { Grid } from "@mui/material";

const NotificationPage = () => {
    return (
        <>
            <Head>
                <title>Success Member Parking</title>
            </Head>
            <MainLayout>
                <Grid style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: "50%"
                }}>
                    <Image src="/images/success.png" width={261} height={334} />
                </Grid>
                <Grid style={{
                    marginTop: "27px",
                    marginBottom: "23px",
                    color: "#2B4CAB",
                    fontSize: "14px",
                    fontWeight: "bold",
                    textAlign: "center",
                    fontFamily: "Poppins"
                }}>Selamat</Grid>
                <Grid style={{
                    color: "#000000",
                    fontSize: "12px",
                    textAlign: "center",
                    fontFamily: "Poppins",
                }}>Selamat status kartu member anda sudah aktif. silahkan ambil kartu member sesuai lokasi yang sudah anda isi sebelumnya</Grid>
            </MainLayout>
        </>
    );
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(NotificationPage);