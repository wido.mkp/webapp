import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./Profile.module.js";
import TextFieldModule from "@/components/TextField";
import Button from '@mui/material/Button';
import MainLayout from "@/containers/MainLayout";
import { Box, Grid } from "@mui/material";
import { useTheme } from "@mui/styles";

const ProfilePage = () => {
    const theme = useTheme();
    const styles = useStyles(theme);
    return (
        <>
            <Head>
                <title>Profile Member Parking</title>
            </Head>
            <MainLayout>
                <Box
                    component="form"
                    noValidate
                    autoComplete="off"
                    onSubmit={(e) => {
                        e.preventDefault();
                    }}>
                    <Grid sx={styles.profileTitle}>
                        Profile
                    </Grid>
                    <Grid sx={styles.profileInputWrapper}>
                        <Grid sx={styles.profileLabel}>Nama</Grid>
                        <TextFieldModule
                            hiddenLabel={true}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid sx={styles.profileInputWrapper}>
                        <Grid sx={styles.profileLabel}>Username</Grid>
                        <TextFieldModule
                            hiddenLabel={true}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid sx={styles.profileInputWrapper}>
                        <Grid sx={styles.profileLabel}>No. Handphone</Grid>
                        <TextFieldModule
                            hiddenLabel={true}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid sx={styles.profileInputWrapper}>
                        <Grid sx={styles.profileLabel}>Email</Grid>
                        <TextFieldModule
                            hiddenLabel={true}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid sx={styles.profileActionWrapper}>
                        <Grid sx={styles.profileLogout}>Log Out</Grid>
                        <Button
                            type="submit"
                            variant="contained"
                            style={{
                                width: "114px",
                                height: "44px",
                                backgroundColor: "#2B4CAB",
                                fontSize: "10px",
                                textTransform: "capitalize"
                            }}
                        >
                            Edit
                        </Button>
                    </Grid>
                </Box>
            </MainLayout>
        </>
    );
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(ProfilePage);