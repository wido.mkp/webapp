const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    profileTitle: {
        fontSize: "14px",
        color: "#2B4CAB",
        fontFamily: "Poppins",
        fontWeight: "bold",
        marginTop: "158px",
    },
    profileInputWrapper: {
        marginTop: "16px",
        display: "flex",
        flexDirection: "column",
        width: "100%",
    },
    profileLabel: {
        color: "#2B4CAB",
        fontSize: "10px",
        fontFamily: "Poppins",
        fontWeight: "bold",
        marginBottom: "5px",
    },
    profileActionWrapper: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
        marginTop: "41px",
    },
    profileLogout: {
        fontFamily: "Poppins",
        fontSize: "10px",
        fontWeight: "bold",
        color: "#D90000",
        cursor: "pointer",
    }
});
export default useStyles;