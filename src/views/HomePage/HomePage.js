import React, { useRef } from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./Home.module.js";
import Image from "next/image";
import Divider from "@/components/Divider";
import HistoryCardItem from "@/components/HistoryCardItem";
import Router from "next/router";
import MainLayout from "@/containers/MainLayout";
import { useTheme } from "@mui/styles";
import Grid from "@mui/material/Grid";

const notifData = [1, 2, 3, 4, 5];
const cardData = [1, 2, 4, 4, 6, 7];
const HomePage = () => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const goHistory = () => {
        Router.push("/home/history");
    }
    const goMember = () => {
        Router.push("/member/dashboard");
    }
    const goExpandMember = () => {
        Router.push("/home/expand_member");
    }
    const goVehicle = () => {
        Router.push("/vehicle/dashboard");
    }
    return (
        <>
            <Head>
                <title>Home Member Parking</title>
            </Head>
            <MainLayout>
                <Grid sx={styles.homeUsername}>
                    <Grid sx={styles.homeGreetingLabel}>Selamat Datang</Grid>
                    <Grid sx={styles.homeGreetingUsername}>
                        John Doe
                    </Grid>
                </Grid>
                <Grid sx={styles.homeFeatureWrapper}>
                    <Grid sx={styles.homeBoxFeature} onClick={() => goMember()}>
                        <Image src="/images/member.png" width={73.4} height={39.22} layout="fixed" />
                        <Grid sx={styles.homeLabelFeature}>
                            Member
                        </Grid>
                    </Grid>
                    <Grid sx={styles.homeBoxFeature} onClick={() => goVehicle()}>
                        <Image src="/images/kendaraan.png" width={73.4} height={39.22} layout="fixed" />
                        <Grid sx={styles.homeLabelFeature}>
                            Kendaraan
                        </Grid>
                    </Grid>
                </Grid>
                <Grid style={{
                    fontSize: "14px",
                    fontFamily: "Poppins",
                    color: "#2B4CAB",
                    fontWeight: "bold",
                    marginTop: "35px",
                }}>Pemberitahuan</Grid>
                <Grid sx={styles.homeNoticeWrapper}>
                    {
                        notifData.map((item, i) => {
                            return (
                                <Grid key={i} sx={styles.homeNoticeLabel} onClick={() => goExpandMember()}>
                                    <Grid sx={styles.homeNoticeCircle}>!</Grid>
                                    <>Member ID. 9310000313881 Avanza H 1234 KL Almost Expired</>
                                </Grid>
                            );
                        })
                    }
                </Grid>
                <Grid style={{ marginTop: "30px", marginLeft: "-40px", marginRight: "-40px" }}>
                    <Divider height="6px" />
                </Grid>
                <Grid sx={styles.homeHistoryWrapper}>
                    <Grid sx={styles.homeHistoryLabel}>
                        <Grid style={{
                            fontSize: "14px",
                            fontFamily: "Poppins",
                            color: "#2B4CAB",
                            fontWeight: "bold",
                        }}>History Pembelian</Grid>
                        <Grid style={{
                            fontSize: "14px",
                            fontFamily: "Poppins",
                            color: "#2B4CAB",
                            fontWeight: "bold",
                            cursor: "pointer"
                        }}
                            onClick={() => goHistory()}
                        >See All</Grid>
                    </Grid>
                    {
                        cardData.map((data, i) => {
                            return (
                                <HistoryCardItem key={i} vehicleName="avanza" date="22/12/2019">
                                    <Grid>
                                        ID. 9310000313881
                                    </Grid>
                                    <Grid>
                                        Avanza H 1234 KL
                                    </Grid>
                                    <Grid>
                                        DP Mall jl. Lorem Ipsum dolor sit amet,
                                    </Grid>
                                    <Grid>
                                        consectetur adipiscing elit......
                                    </Grid>
                                </HistoryCardItem>
                            );
                        })
                    }
                </Grid>
            </MainLayout>
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(HomePage);