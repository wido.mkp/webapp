import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./Register.module.js";
import Button from '@mui/material/Button';
import TextFieldModule from "@/components/TextField";
import Router from "next/router";
import ResponseModal from "@/components/ResponseModal";
import { Box, Container, Grid } from "@mui/material";
import { useTheme } from '@mui/material/styles';
import LoadingScreen from "@/components/LoadingScreen/LoadingScreen.js";

const RegisterPage = () => {
    const [open, setOpen] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const theme = useTheme();
    const styles = useStyles(theme);
    React.useEffect(() => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
        }, 500)
    }, []);
    if (loading) {
        return (
            <LoadingScreen />
        )
    }
    const toLoginPage = () => {
        handleOpen();
        setTimeout(() => {
            handleClose();
            Router.push("/auth/login");
        }, 1000);
    }
    return (
        <>
            <Head>
                <title>Signup</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Grid sx={styles.container}>
                <Grid sx={styles.registerWrapper}>
                    <Grid sx={styles.registerHeader} />
                    <Box
                        component="form"
                        noValidate
                        autoComplete="off"
                        onSubmit={(e) => {
                            e.preventDefault();
                            toLoginPage();
                        }}
                        sx={styles.registerContent}>
                        <Grid sx={styles.registerLabelSign}>SIGN UP</Grid>
                        <Grid sx={styles.registerInputWrapper}>
                            <TextFieldModule
                                label="Nama Depan"
                                variant="outlined"
                                width="276px"
                            />
                        </Grid>
                        <Grid sx={styles.registerInputWrapper}>
                            <TextFieldModule
                                label="Nama Belakang"
                                variant="outlined"
                                width="276px"
                            />
                        </Grid>
                        <Grid sx={styles.registerInputWrapper}>
                            <TextFieldModule
                                label="Username"
                                variant="outlined"
                                width="276px"
                            />
                        </Grid>
                        <Grid sx={styles.registerInputWrapper}>
                            <TextFieldModule
                                label="No Handphone"
                                variant="outlined"
                                width="276px"
                            />
                        </Grid>
                        <Grid sx={styles.registerInputWrapper}>
                            <TextFieldModule
                                label="Email"
                                variant="outlined"
                                width="276px"
                            />
                        </Grid>
                        <Grid sx={styles.registerInputWrapper}>
                            <TextFieldModule
                                label="Password"
                                variant="outlined"
                                type="password"
                                width="276px"
                            />
                        </Grid>
                        <Grid sx={styles.registerInputWrapper}>
                            <TextFieldModule
                                label="Konfirmasi Password"
                                variant="outlined"
                                type="password"
                                width="276px"
                            />
                        </Grid>
                        <Grid style={{ marginTop: "29px" }}>
                            <Button
                                variant="contained"
                                type="submit"
                                style={{
                                    width: "277px",
                                    height: "48px",
                                    backgroundColor: "#2B4CAB",
                                    fontSize: "10px",
                                    textTransform: "capitalize"
                                }}
                                onClick={() => toLoginPage()}
                            >
                                Sign Up
                            </Button>
                        </Grid>
                    </Box>
                    <Grid sx={styles.registerCopyright}>
                        <Grid style={{ color: "#C4C4C4" }}>Copyright by MKP E-TICKETING</Grid>
                    </Grid>
                </Grid>
                <ResponseModal
                    open={open}
                    onClose={handleClose}
                    icon="/images/message.png"
                    title="registrasi berhasil"
                    content="Selamat anda sudah berhasil registrasi. silahkan cek email untuk mengaktifkan akun anda"
                />
            </Grid>
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(RegisterPage);