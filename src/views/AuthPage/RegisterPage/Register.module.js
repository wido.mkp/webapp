const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100%',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    registerWrapper:{
        backgroundColor: '#FFFFFF',
        position: 'relative',
        height: '100%',
    },
    registerHeader:{
        width: '100%',
        height: '300px',
        objectFit: 'cover',
        marginTop: '-30px',
    },
    registerContent:{
        marginTop: '-230px',
    },
    registerInputWrapper:{
        marginBottom: '27px',
    },
    registerCopyright:{
        marginTop: "105px",
        fontSize: '10px',
    },
    registerLabelSign:{
        fontSize: '14px',
        color: '#2B4CAB',
        fontWeight: 'bold',
        marginBottom: '10%'
    }
});
export default useStyles;