const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100%',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    loginWrapper: {
        display: 'block',
        backgroundColor: '#FFFFFF',
        height: '100%',
        position: 'relative',
        width: '100%',
        overflowY: 'scroll'
    },
    loginImage: {
        width: '100%',
        height: '30%',
        objectFit: 'contain',
    },
    loginHeader: {
        width: '100%',
        height: '300px',
        objectFit: 'cover',
        marginTop: '-30px',
    },
    loginContent: {
        flexDirection: 'column',
        paddingTop: '10%',
        height: "350px",
    },
    loginInputWrapper: {
        paddingTop: '32px'
    },
    loginInput: {
        width: '276px'
    },
    loginForgotWrapper: {
        marginTop: '11px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        fontSize: '10px'
    },
    loginRegisterWrapper: {
        marginTop: '25px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        fontSize: '10px'
    },
    loginCopyright: {
        marginTop: "40px",
        fontSize: '10px',
    }
});
export default useStyles;