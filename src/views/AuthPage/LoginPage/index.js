import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./Login.module.js";
import Button from '@mui/material/Button';
import TextFieldModule from "@/components/TextField";
import Router from "next/router";
import { Box, Container, Grid } from "@mui/material";
import { useTheme } from '@mui/material/styles';
import CardMedia from '@mui/material/CardMedia';

const LoginPage = () => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const toRegisterPage = () => {
        Router.push("/auth/signup");
    }
    const toForgetPage = () => {
        Router.push("/auth/forget");
    }
    const toHomePage = () => {
        setTimeout(() => {
            Router.push("/home/dashboard")
        }, 1000)
    }
    return (
        <>
            <Head>
                <title>Login Member Parking</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Grid sx={styles.container}>
                <Grid sx={styles.loginWrapper}>
                    <CardMedia
                        component="img"
                        image="/images/header_login.png"
                        alt="login-header"
                        sx={styles.loginHeader}
                        loading="lazy" />
                    <Box
                        component="form"
                        noValidate
                        autoComplete="off"
                        onSubmit={(e) => {
                            e.preventDefault();
                            toHomePage();
                        }}
                        sx={styles.loginContent}>
                        <Grid sx={styles.loginInputWrapper}>
                            <TextFieldModule
                                label="Username/Email"
                                variant="outlined"
                                width="276px"
                                id="username"
                            />
                        </Grid>
                        <Grid sx={styles.loginInputWrapper}>
                            <TextFieldModule
                                label="Password"
                                variant="outlined"
                                width="276px"
                                id="password"
                            />
                        </Grid>
                        <Grid sx={styles.loginForgotWrapper}>
                            <Grid style={{ color: "black", fontWeight: "bold", marginRight: "5px" }}>Forgot your password?</Grid>
                            <Grid style={{ color: "#0398fc", cursor: "pointer" }} onClick={() => toForgetPage()}>Click here</Grid>
                        </Grid>
                        <Grid style={{ marginTop: "31px" }}>
                            <Button
                                type="submit"
                                variant="contained"
                                style={{
                                    width: "225px",
                                    height: "48px",
                                    backgroundColor: "#2B4CAB",
                                    fontSize: "10px",
                                    textTransform: "capitalize"
                                }}
                                onClick={() => toHomePage()}
                            >
                                Log In
                            </Button>
                        </Grid>
                        <Grid sx={styles.loginRegisterWrapper}>
                            <Grid style={{ color: "black", fontWeight: "bold", marginRight: "5px" }}>Don’t  have an account?</Grid>
                            <Grid
                                onClick={() => toRegisterPage()}
                                style={{ color: "#0398fc", cursor: "pointer" }}
                            >
                                Register now.
                            </Grid>
                        </Grid>
                    </Box>
                    <Grid sx={styles.loginCopyright}>
                        <Grid style={{ color: "#C4C4C4" }}>Copyright by MKP E-TICKETING</Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(LoginPage);