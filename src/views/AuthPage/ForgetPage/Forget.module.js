const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    forgetWrapper:{
        display: 'block',
        backgroundColor: '#FFFFFF',
        height: '100%',
        position: 'relative',
        width: '100%',
        overflowY: 'scroll'
    },
    forgetImage:{
        width: '100%',
        height: '30%',
        objectFit: 'cover',
        flex: 1,
    },
    forgetHeader:{
        width: '100%',
        height: '300px',
        objectFit: 'cover',
        marginTop: '-30px',
    },
    forgetContent:{
        paddingTop: '10%',
        height: "350px",
    },
    forgetInputWrapper:{
        paddingTop: '32px',
    },
    forgetInput:{
        width: '276px',
    },
    forgetForgotWrapper:{
        marginTop: '11px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        fontSize: '10px',
    },
    forgetforgetWrapper:{
        marginTop: '25px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        fontSize: '10px',
    },
    forgetCopyright:{
        marginTop: "40px",
        fontSize: '10px',
    }
});
export default useStyles;