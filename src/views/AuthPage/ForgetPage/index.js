import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import Button from '@mui/material/Button';
import TextFieldModule from "@/components/TextField";
import ResponseModal from "@/components/ResponseModal";
import Router from "next/router";
import { Box, Container, Grid } from "@mui/material";
import { useTheme } from '@mui/material/styles';
import useStyles from './Forget.module';
import CardMedia from '@mui/material/CardMedia';
import LoadingScreen from "@/components/LoadingScreen/LoadingScreen";

const ForgetPage = () => {
    const [open, setOpen] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const theme = useTheme();
    const styles = useStyles(theme);
    React.useEffect(() => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
        }, 500)
    }, []);
    if (loading) {
        return (
            <LoadingScreen />
        )
    }
    const toLoginPage = () => {
        handleOpen();
        setTimeout(() => {
            handleClose();
            Router.push("/auth/login");
        }, 1000);
    }
    return (
        <>
            <Head>
                <title>Forget Member Parking</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Grid sx={styles.container}>
                <Grid sx={styles.forgetWrapper}>
                    <CardMedia
                        component="img"
                        image="/images/header_login.png"
                        alt="login-header"
                        sx={styles.forgetHeader}
                        loading="lazy" />
                    <Box
                        component="form"
                        noValidate
                        autoComplete="off"
                        onSubmit={(e) => {
                            e.preventDefault();
                            toLoginPage();
                        }}
                        sx={styles.forgetContent}>
                        <Grid sx={styles.forgetInputWrapper}>
                            <TextFieldModule
                                label="Masukkan Email"
                                variant="outlined"
                                width="276px"
                            />
                        </Grid>
                        <Grid style={{ marginTop: "80px" }}>
                            <Button
                                variant="contained"
                                type="submit"
                                style={{
                                    width: "225px",
                                    height: "48px",
                                    backgroundColor: "#2B4CAB",
                                    fontSize: "10px",
                                }}
                                onClick={toLoginPage}
                            >
                                OK
                            </Button>
                        </Grid>
                    </Box>
                    <Grid sx={styles.forgetCopyright}>
                        <Grid style={{ color: "#C4C4C4" }}>Copyright by MKP E-TICKETING</Grid>
                    </Grid>
                </Grid>
            </Grid>
            <ResponseModal
                open={open}
                onClose={handleClose}
                icon="/images/message.png"
                title="PERMINTAAN GANTI PASSWORD"
                content="Notifikasi email terkirim. Silahkan cek email untuk melanjutkan langkah penggantian password akun anda"
            />
        </>
    );
};

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(ForgetPage);