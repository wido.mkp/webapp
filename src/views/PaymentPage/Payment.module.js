const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    labelMember:{
        fontFamily: "Poppins",
        fontSize: "14px",
        color: "#2B4CAB",
        fontWeight: "bold",
        marginBottom: "20px",
    }
});
export default useStyles;