import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import MainLayout from "@/containers/MainLayout";
import useStyles from "./Payment.module.js";
import HistoryCardItem from "@/components/HistoryCardItem";
import Button from '@mui/material/Button';
import Router from "next/router";
import { useTheme } from "@mui/styles";
import { Grid } from "@mui/material";

const PaymentPage = () => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const goSuccesPage = () => {
        Router.push("/home/notification");
    }
    return (
        <>
            <Head>
                <title>Payment Member Parking</title>
            </Head>
            <MainLayout>
                <Grid style={{ marginTop: "53.4px" }}>
                    <Grid sx={styles.labelMember}>
                        Cart
                    </Grid>
                    <HistoryCardItem
                        vehicleName="avanza"
                        isShowDate={false}
                        icon="motorcycle"
                    >
                        <Grid>
                            ID. 9310000313881
                        </Grid>
                        <Grid>
                            Avanza H 1234 KL
                        </Grid>
                        <Grid>
                            DP Mall jl. Lorem Ipsum dolor sit amet,
                        </Grid>
                        <Grid>
                            consectetur adipiscing elit......
                        </Grid>
                        <Grid style={{
                            display: "flex",
                            flexDirection: "column",
                            fontFamily: "Poppins",
                            fontSize: "9px",
                            marginTop: "10px"
                        }}>
                            <Grid style={{ color: "#2B4CAB", fontWeight: "bold" }}>Member 3 Bulan</Grid>
                            <Grid>Start from. 31/03/2022</Grid>
                            <Grid>To. 01/07/2022</Grid>
                        </Grid>
                        <Grid style={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            width: "90%",
                            marginTop: "20px",
                            marginBottom: "15px",
                            alignItems: "center"
                        }}>
                            <Grid style={{ color: "#2B4CAB" }}>Total Pembayaran</Grid>
                            <Grid style={{
                                color: "#2B4CAB",
                                fontSize: "16px",
                                fontWeight: "bold"
                            }}>Rp 100.000,-</Grid>
                        </Grid>
                    </HistoryCardItem>
                    <Grid style={{
                        marginTop: "46px"
                    }}>
                        <Grid sx={styles.labelMember}>
                            Pilih Pembayaran
                        </Grid>
                    </Grid>
                    <Button
                        fullWidth
                        variant="contained"
                        style={{
                            height: "49px",
                            backgroundColor: "#FFFFFF",
                            fontSize: "14px",
                            color: "#2B4CAB",
                            borderRadius: "6px",
                            display: "flex",
                            justifyContent: "flex-start",
                            alignItems: "center",
                            paddingLeft: "50px",
                            fontWeight: "bold",
                            marginBottom: "19px"
                        }}
                    >
                        Qris
                    </Button>
                    <Button
                        fullWidth
                        variant="contained"
                        style={{
                            height: "49px",
                            backgroundColor: "#FFFFFF",
                            fontSize: "14px",
                            color: "#2B4CAB",
                            borderRadius: "6px",
                            display: "flex",
                            justifyContent: "flex-start",
                            alignItems: "center",
                            paddingLeft: "50px",
                            fontWeight: "bold",
                            marginBottom: "19px"
                        }}
                    >
                        Virtual Account
                    </Button>
                    <Button
                        fullWidth
                        variant="contained"
                        style={{
                            height: "49px",
                            backgroundColor: "#FFFFFF",
                            fontSize: "14px",
                            color: "#2B4CAB",
                            borderRadius: "6px",
                            display: "flex",
                            justifyContent: "flex-start",
                            alignItems: "center",
                            paddingLeft: "50px",
                            fontWeight: "bold",
                            marginBottom: "19px"
                        }}
                    >
                        Credit Card
                    </Button>
                    <Grid style={{
                        marginTop: "46px",
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                    }}>
                        <Button
                            variant="contained"
                            style={{
                                width: "184px",
                                height: "42px",
                                backgroundColor: "#2B4CAB",
                                fontSize: "14px",
                                color: "white",
                                borderRadius: "6px",
                                fontWeight: "bold",
                                marginBottom: "70px"
                            }}
                            onClick={() => goSuccesPage()}
                        >
                            OK
                        </Button>
                    </Grid>
                </Grid>
            </MainLayout>
        </>
    )
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(PaymentPage);