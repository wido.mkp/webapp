import React from "react";
import { connect } from "react-redux";
import Head from "next/head";
import useStyles from "./RegisterVehicle.module.js";
import MainLayout from "@/containers/MainLayout";
import Image from "next/image";
import Divider from "@/components/Divider";
import SelectModule from "@/components/Select";
import TextFieldModule from "@/components/TextField";
import Button from '@mui/material/Button';
import { useTheme } from "@mui/styles";
import { Grid } from "@mui/material";

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];
const VechicleRegisterPage = () => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [personName, setPersonName] = React.useState([]);
    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    return (
        <>
            <Head>
                <title>Register Vehicle Member Parking</title>
            </Head>
            <MainLayout>
                <Grid style={{ marginTop: "55px" }}>
                    <Grid sx={styles.labelMember}>
                        Member
                    </Grid>
                    <Grid sx={styles.statusMemberWrapper}>
                        <Grid sx={styles.boxImage}>
                            <Image src="/images/car.png" width={86.1} height={46} layout="fixed" />
                        </Grid>
                        <Grid sx={styles.boxStatus}>
                            <Grid sx={styles.statusLabel}>STATUS MEMBER</Grid>
                            <Grid sx={styles.statusItemWrapper}>
                                <Grid sx={styles.circleItem} />
                                <Grid sx={styles.itemVehicle}>2 Cars</Grid>
                            </Grid>
                            <Grid sx={styles.statusItemWrapper}>
                                <Grid sx={styles.circleItem} />
                                <Grid sx={styles.itemVehicle}>4 Motorcycles</Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid style={{ marginLeft: "-40px", marginRight: "-40px", marginBottom: "35px" }}>
                        <Divider height="6px" />
                    </Grid>
                    <Grid sx={styles.labelMember}>
                        Tambah Kendaraan
                    </Grid>
                    <SelectModule
                        label="Pilih Jenis Kendaraan"
                        data={names}
                        value={personName}
                        width="100%"
                        onChange={handleChange}
                        style={{ height: "31px", fontSize: "9px", marginBottom: "20px" }}
                    />
                    <TextFieldModule
                        label="No Plat Kendaraan"
                        variant="outlined"
                        fullWidth={true}
                    />
                    <Grid style={{ marginTop: "20px" }}>
                        <TextFieldModule
                            label="Keterangan Nama Kendaraan"
                            variant="outlined"
                            fullWidth={true}
                        />
                    </Grid>
                    <Grid style={{
                        marginTop: "53px",
                        marginBottom: "176px",
                        width: "100%",
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center"
                    }}>
                        <Button
                            variant="contained"
                            style={{
                                width: "184px",
                                height: "42px",
                                backgroundColor: "#2B4CAB",
                                fontSize: "10px",
                                textTransform: "capitalize",
                                color: "white",
                                borderRadius: "6px"
                            }}
                        >
                            SIMPAN
                        </Button>
                    </Grid>
                </Grid>
            </MainLayout>
        </>
    )
}

const mapStateToProps = (state) => {
    let { rootReducer } = state;
    return {
        ...rootReducer,
    };
};
export default connect(mapStateToProps)(VechicleRegisterPage);