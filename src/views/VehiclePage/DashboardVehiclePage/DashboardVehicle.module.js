const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    labelMember: {
        fontFamily: "Poppins",
        fontSize: "14px",
        color: "#2B4CAB",
        fontWeight: "bold",
        marginBottom: "20px",
    },
    statusMemberWrapper: {
        marginTop: "44px",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: "38px",
    },
    boxImage: {
        backgroundColor: "#2B4CAB",
        width: "121px",
        height: "121px",
        borderRadius: "6px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    boxStatus: {
        width: "121px",
        height: "121px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        position: "relative",
    },
    statusLabel: {
        fontFamily: "Poppins",
        fontSize: "10px",
        color: "#2B4CAB",
        fontWeight: "bold",
        marginBottom: "10px",
    },
    statusItemWrapper: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginBottom: "3px",
    },
    circleItem: {
        width: "9px",
        height: "9px",
        borderRadius: "4.5px",
        backgroundColor: "black",
    },
    itemVehicle: {
        fontFamily: "Poppins",
        fontSize: "9px",
        color: "black",
        marginLeft: "10px",
    }
});
export default useStyles;