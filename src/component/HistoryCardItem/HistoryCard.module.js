const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        position: 'relative',
        backgroundColor: '#fafafa',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    vehicleLabelIcon: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    cardContent: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start",
        fontSize: "9px",
        color: "#000000",
        paddingLeft: "15px",
        paddingTop: "15px",
        paddingBottom: "15px",
        fontWeight: "400",
        fontFamily: "Poppins",
    },
    cardExpand: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start",
        paddingLeft: "15px",
        paddingBottom: "5px",
        fontSize: "9px",
        color: "#000000",
        fontWeight: "400",
        fontFamily: "Poppins",
    }
});
export default useStyles;