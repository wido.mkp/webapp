import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
const SelectModule = ({ label, data, onChange, value, style, width }) => {
    return (
        <FormControl sx={{ width: width, mt: 3 }} size="small">
            <Select
                displayEmpty
                value={value}
                onChange={onChange}
                input={<OutlinedInput />}
                style={style}
                renderValue={(selected) => {
                    if (selected.length === 0) {
                        return <div style={{ fontSize: "9px" }}>{label}</div>;
                    }

                    return selected.join(', ');
                }}
                inputProps={{ 'aria-label': 'Without label' }}
            >
                <MenuItem disabled value="">
                    <div style={{ fontSize: "9px" }}>{label}</div>
                </MenuItem>
                {data.map((name) => (
                    <MenuItem
                        key={name}
                        value={name}
                        style={{ fontSize: "9px" }}
                    >
                        {name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
}

export default SelectModule;