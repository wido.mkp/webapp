import { TextField } from '@mui/material';
import React from 'react';

const TextFieldModule = (props) => {
    return (
        <>
            <TextField
                id={props.id ? props.id : "textfield"}
                fullWidth={props.fullWidth ? props.fullWidth : false}
                value={props.value}
                hiddenLabel={props.hiddenLabel ? props.hiddenLabel : false}
                label={props.label}
                variant={props.variant}
                InputLabelProps={{
                    style: {
                        fontSize: "10px",
                    }
                }}
                InputProps={{
                    style: {
                        fontSize: "10px",
                        width: props.width ? props.width : "100%",
                    },
                }}
                type={props.type ? props.type : "text"}
                size="small"
                {
                ...props
                }
            />
        </>
    );
};

export default TextFieldModule;