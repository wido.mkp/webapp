import Grid from "@mui/material/Grid";
import BottomNavigationModule from "../BottomNavigationModule";
import useStyles from "./MainLayout.module.js";
import { useTheme } from "@mui/styles";
import React from "react";
import LoadingScreen from "@/components/LoadingScreen/LoadingScreen";

const MainLayout = (props) => {
    const theme = useTheme();
    const styles = useStyles(theme);
    const [loading, setLoading] = React.useState(false);
    React.useEffect(() => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
        }, 500)
    }, []);
    if (loading) {
        return (
            <LoadingScreen />
        )
    }
    return (
        <>
            <Grid sx={styles.container}>
                <Grid sx={styles.layoutWrapper}>
                    <Grid sx={styles.layoutHeaderWrapper}>
                        <Grid sx={styles.bgWrapper} />
                        {
                            props.children
                        }
                    </Grid>
                </Grid>
                {
                    props.disableNav && props.disableNav === true ? null : <BottomNavigationModule />
                }
            </Grid>
        </>
    );
}

export default MainLayout