const useStyles = theme => ({
    container: {
        margin: 'auto',
        padding: 0,
        width: '360px',
        height: '100vh',
        textAlign: 'center',
        backgroundColor: '#fafafa',
        position: 'fixed',
        marginLeft: 'auto',
        marginRight: 'auto',
        left: 0,
        right: 0,
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            backgroundColor: '#fafafa'
        },
    },
    layoutWrapper:{
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        height: '100%',
        textAlign: 'left',
        overflowY: 'scroll',
    },
    layoutHeaderWrapper:{
        paddingLeft: '40px',
        paddingRight: '40px',
    },
    bgWrapper : {
        background: `url("/images/bg.png")`,
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '30%',
        right:0,
        left: 0,
        display: 'flex',
        flexDirection: 'column',
        top: '0',
        position: 'absolute',
        objectFit: 'contain',
        backgroundSize: '100% 60%',
    }
});
export default useStyles;