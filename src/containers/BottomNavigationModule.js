import React from "react";
import Paper from '@mui/material/Paper';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import HomeIcon from '@mui/icons-material/Home';
import FeedIcon from '@mui/icons-material/Feed';
import ElectricRickshawIcon from '@mui/icons-material/ElectricRickshaw';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { useRouter } from 'next/router'
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

const BottomNavigationModule = () => {
    const router = useRouter()
    const [value, setValue] = React.useState(0);
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
    React.useEffect(() => {
        getPathUrl();
    }, [])
    const getPathUrl = () => {
        let pathname = router.pathname;
        if (pathname.indexOf("home") !== -1) {
            setValue(0)
        } else if (pathname.indexOf("member") !== -1) {
            setValue(1)
        } else if (pathname.indexOf("vehicle") !== -1) {
            setValue(2)
        } else if (pathname.indexOf("profile") !== -1) {
            setValue(3)
        }
    }
    return (
        <Paper sx={{
            position: "fixed",
            bottom: 0,
            right: 0,
            left: 0,
            width: isMobile ? "100%" : "360px",
            marginLeft: "auto",
            marginRight: "auto"
        }} elevation={3}>
            <BottomNavigation
                showLabels
                value={value}
                onChange={(event, newValue) => {
                    setValue(newValue);
                    if (newValue === 0) {
                        router.push("/home/dashboard");
                    } else if (newValue === 1) {
                        router.push("/member/dashboard");
                    } else if (newValue === 2) {
                        router.push("/vehicle/dashboard");
                    } else if (newValue === 3) {
                        router.push("/profile")
                    }
                }}
            >
                <BottomNavigationAction label={<div style={{ fontSize: "10px", marginTop: "2px" }}>Home</div>} icon={<HomeIcon />} />
                <BottomNavigationAction label={<div style={{ fontSize: "10px", marginTop: "2px" }}>Member</div>} icon={<FeedIcon />} />
                <BottomNavigationAction label={<div style={{ fontSize: "10px", marginTop: "2px" }}>Kendaraan</div>} icon={<ElectricRickshawIcon />} />
                <BottomNavigationAction label={<div style={{ fontSize: "10px", marginTop: "2px" }}>Profil</div>} icon={<AccountCircleIcon />} />
            </BottomNavigation>
        </Paper>
    );
}

export default BottomNavigationModule;