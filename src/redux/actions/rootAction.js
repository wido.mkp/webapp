export const addClickToGlobal = props => {
  return props.dispatch(
    {
      type: 'ADD_CLICK_TO_GLOBAL',
      payload: props.body,
    }
  );
};
export const addMessageNfc = props => {
  return props.dispatch(
    {
      type: 'ADD_MESSAGE_NFC',
      payload: props.body,
    }
  );
};
