/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['www.gotravelly.com'],
  },
}

module.exports = nextConfig
